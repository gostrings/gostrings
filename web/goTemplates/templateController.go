package goTemplates

import (
	"github.com/GoStrings/service"
	"html/template"
	"net/http"
)

type TemplateController interface {
	Home(w http.ResponseWriter, r *http.Request)
}

type templateController struct {
	stringsService service.StringsService
}

func NewTemplateController(stringsService service.StringsService) TemplateController {
	return &templateController{
		stringsService: stringsService,
	}
}

func (t templateController) Home(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		signupTemp, _ := template.ParseFiles("./web/goTemplates/html/strings.html")
		err := signupTemp.Execute(w, nil)
		if err != nil {
			return
		}

	}
}
