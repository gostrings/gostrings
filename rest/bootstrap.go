package rest

import (
	"github.com/GoStrings/logger"
	"github.com/GoStrings/service"
	"github.com/GoStrings/web/goTemplates"
)

func StartServer(container *service.Container) *HttpServer {
	// Inject services instance from ServiceContainer

	// Initializing Controllers
	stringsController := NewStringsController(container.StringsService)
	templateController := goTemplates.NewTemplateController(container.StringsService)

	//Initializing http server
	httpServer := NewHttpServer(
		container.GbeConfigService.GetConfig().RestServer.Addr,
	)

	//Inject controller instance to server
	httpServer.stringsController = stringsController
	httpServer.templateController = templateController
	go httpServer.Start()
	logger.Instance().Info("rest server ok")
	return httpServer
}
