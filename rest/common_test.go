package rest

import (
	"github.com/GoStrings/mocks/service"
	"github.com/GoStrings/models"
	"github.com/golang/mock/gomock"
	"testing"
)

type TestService interface {
	GetMockStringsService() *service.MockStringsService
}

type testService struct {
	ctrl *gomock.Controller
}

func (t *testService) GetMockStringsService() *service.MockStringsService {
	return service.NewMockStringsService(t.ctrl)
}

func NewTestService(t *testing.T) TestService {
	mockCtrl := gomock.NewController(t)
	return &testService{
		ctrl: mockCtrl,
	}
}

// Mocked correct response
func GetMockCorrectInputRequest() *models.InputRequest {
	return &models.InputRequest{
		Input: "123-has-456-d",
	}
}

// Mocked incorrect response
func GetMockIncorrectInputRequest1() *models.InputRequest {
	return &models.InputRequest{
		Input: "123a-ha!s-456d-11-addaasss-1-qwer-1-z",
	}
}

// Mocked story pack
func GetMockStoryPack() *models.StoryStatsPack {
	return &models.StoryStatsPack{
		AverageLength: 2,
		List:          " has d",
		LongestWord:   "has",
		ShortestWord:  "d",
	}
}

// Another Mocked Story Pack
func GetMockStoryPack2() *models.StoryStatsPack {
	return &models.StoryStatsPack{
		AverageLength: 2,
		List:          " has d",
		LongestWord:   "has",
		ShortestWord:  "r",
	}
}

// Mocked standard response
func GetMockStandardResp() *models.StandardResponse {
	return models.NewStandardResponse(false, 400, "String is invalid", nil)
}
