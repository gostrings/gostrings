package rest

import (
	"encoding/json"
	"github.com/GoStrings/models"
	"github.com/GoStrings/service"
	"io/ioutil"
	"net/http"
	"strings"
)

type StringsController interface {
	GetInputString(w http.ResponseWriter, r *http.Request)
}

type stringsController struct {
	stringsService service.StringsService
}

func NewStringsController(userService service.StringsService) StringsController {
	return &stringsController{
		stringsService: userService,
	}
}

func (s stringsController) GetInputString(w http.ResponseWriter, r *http.Request) {
	// adding content-type application/json to header
	w.Header().Add("Content-Type", "application/json")

	// reads all from request body
	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusInternalServerError, "Internal Server Error", nil), http.StatusOK) //nolint:typecheck
		return
	}
	// initializing a var of type InputRequest
	var req models.InputRequest
	// unmarshalling the bodyBytes into the req, thus copying the data
	err = json.Unmarshal(bodyBytes, &req)
	// if err, throw error
	if err != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusInternalServerError, "Internal Server Error", nil), http.StatusOK) //nolint:typecheck
		return
	}
	// if input string is empty, returns an error
	if len(strings.TrimSpace(req.Input)) == 0 {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusBadRequest, "String Cannot be empty", nil), http.StatusOK) //nolint:typecheck
		return
	}
	// calling service to perform operations on the user input
	avg, wholeStory, storyStats, sr := s.stringsService.GetInputString(req)
	// if any standard response is generated, it is thrown as error
	if sr != nil {
		// writing the standard response
		WriteErrorJson(w, sr, http.StatusOK) //nolint:typecheck
		return
	}

	resp := GetResponse(avg, wholeStory, storyStats)
	// success
	WriteJson(w, models.NewStandardResponse(true, 1000, "Success", resp), http.StatusOK)
}
