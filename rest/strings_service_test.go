package rest

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

// Creating mock test cases with dummy inputs to verify if correct data is returned
func TestDifferntInputStrings(t *testing.T) {
	t.Run("Should return correct data", func(t *testing.T) {
		testService := NewTestService(t)
		mockCorrectInput := GetMockCorrectInputRequest()
		mockService := testService.GetMockStringsService()
		mockStoryPack := GetMockStoryPack()
		mockService.EXPECT().GetInputString(mockCorrectInput).Return(int64(289), " has d", mockStoryPack, nil).AnyTimes()
		resp := GetResponse(int64(289), " has d", mockStoryPack)
		assert.Equal(t, resp, resp, "")
	})
	t.Run("Should return validation error error", func(t *testing.T) {
		testService := NewTestService(t)
		mockIncorrectInput := GetMockIncorrectInputRequest1()
		mockService := testService.GetMockStringsService()
		mockStandardResp := GetMockStandardResp()
		mockService.EXPECT().GetInputString(mockIncorrectInput).Return(int64(0), "", nil, mockStandardResp).AnyTimes()
		assert.Equal(t, mockStandardResp.Message, "String is invalid", "Will return an error")
	})
	t.Run("Should return correct average number", func(t *testing.T) {
		testService := NewTestService(t)
		mockCorrectInput := GetMockCorrectInputRequest()
		mockService := testService.GetMockStringsService()
		mockStoryPack := GetMockStoryPack()
		mockService.EXPECT().GetInputString(mockCorrectInput).Return(int64(289), " has d", mockStoryPack, nil).AnyTimes()
		resp := GetResponse(int64(289), " has d", mockStoryPack)
		assert.Equal(t, resp.AvgNum, int64(289), "289 is mock correct average number")
	})
	t.Run("Should return correct Whole Story", func(t *testing.T) {
		testService := NewTestService(t)
		mockCorrectInput := GetMockCorrectInputRequest()
		mockService := testService.GetMockStringsService()
		mockStoryPack := GetMockStoryPack()
		mockService.EXPECT().GetInputString(mockCorrectInput).Return(int64(289), " has d", mockStoryPack, nil).AnyTimes()
		resp := GetResponse(int64(289), " has d", mockStoryPack)
		assert.Equal(t, resp.StoryStats.AverageLength, 2, "2 is mock correct length")
	})
	t.Run("Should return correct Whole Story", func(t *testing.T) {
		testService := NewTestService(t)
		mockCorrectInput := GetMockCorrectInputRequest()
		mockService := testService.GetMockStringsService()
		mockStoryPack := GetMockStoryPack2()
		mockService.EXPECT().GetInputString(mockCorrectInput).Return(int64(289), " has d", mockStoryPack, nil).AnyTimes()
		resp := GetResponse(int64(289), " has d", nil)
		assert.Equal(t, resp.WholeStory, resp.WholeStory, "Should return mocked story")
	})
}
