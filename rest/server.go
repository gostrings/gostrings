package rest

import (
	"github.com/GoStrings/web/goTemplates"
	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	"net/http"
	"os"
	"time"
)

type HttpServer struct {
	addr               string
	stringsController  StringsController
	templateController goTemplates.TemplateController
}

//NewHttpServer create server instance
func NewHttpServer(addr string,
) *HttpServer {
	return &HttpServer{
		addr: addr,
	}
}
func (s HttpServer) GetTemplateController() goTemplates.TemplateController {
	return s.templateController
}

func (s HttpServer) GetStringsController() StringsController {
	return s.stringsController
}

func (server *HttpServer) Start() {
	r := chi.NewRouter()

	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"*"},
		ExposedHeaders:   []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           int(12 * time.Hour),
	}))

	r.Route("/", func(r chi.Router) {
		r.Post("/sendResp", server.stringsController.GetInputString)
		r.HandleFunc("/", server.templateController.Home)
	})

	port := os.Getenv("PORT")
	if port == "" {
		port = "9000" // Default port if not specified
	}

	err := http.ListenAndServe(":"+port, r)
	if err != nil {
		panic(err)
	}

}
