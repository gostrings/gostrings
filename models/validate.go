package models

import (
	"regexp"
	"strconv"
)

// Validate ->
//	Estimated Time to Complete : 30 minutes approx
//	Used time: 70-120 minutes including commenting due to lots of distractions + accommodating changes
//	Difficulty: Easy
func (i InputRequest) Validate() bool {
	var counter = 2 //nolint:gofumpt
	// string regexp
	stringRe := regexp.MustCompile("^[^0-9]*$")
	// number regexp
	numRe := regexp.MustCompile("[0-9]+")

	//nolint:lll    // regexp for primary validation (is string correct?) and secondary expression (treat every substring as separate element)
	secondaryExpression := regexp.MustCompile("[-]+")

	// separate each substring and save to allElems
	allElems := secondaryExpression.Split(i.Input, -1)

	// iterate over allElems and separate each element
	for _, element := range allElems {
		// if the word is at even position w.r.t counter, it must be a number
		if counter%2 == 0 {
			if !numRe.MatchString(element) {
				return false
			}
			// if the word is not a number, return false
			_, err := strconv.Atoi(element) //nolint:wsl
			if err != nil {                 //nolint:wsl
				return false
			}
		} else { // if the word is at odd position w.r.t counter, it must be a string
			if !stringRe.MatchString(element) {
				return false
			}
			// if word contains a number, return false
			if numRe.MatchString(element) == true {
				return false
			}
		}
		// increment the counter
		counter++
	}
	// return
	return true //nolint:wsl
}
