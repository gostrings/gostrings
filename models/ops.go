package models

import (
	"regexp"
	"strconv"
)

// AverageNumber ->
//	Estimated Time to Complete : 15 minutes approx
//	Used time: 15-20 minutes including commenting
//	Difficulty: Easy
func AverageNumber(input string) int64 {
	// average number is calculated and saved in getAverageNumber variable and returned to caller
	getAverageNumber := 0
	// counter counts the total iterations performed to get an average
	counter := 0
	// initialized regex that only searches for numbers within a string
	re := regexp.MustCompile("[0-9]+")
	// find all successive matches depending upon the regexp initialized above
	allElems := re.FindAllString(input, -1)
	// iterate over allElems and separate each element
	for _, element := range allElems {
		counter++
		// element of type string is converted to number of type int
		element2num, _ := strconv.Atoi(element)
		// saving avg number to a variable
		getAverageNumber += element2num
	}
	// Calculate average of all numbers present in the string
	getAverageNumber /= counter
	// returns the value
	return int64(getAverageNumber)
}

// WholeStory ->
//	Estimated Time to Complete : 15 minutes approx
//	Used time: 10-15 minutes including commenting
//	Difficulty: Easy
func WholeStory(input string) string {
	// resultantString stores the result
	var resultantString string

	// initialized regex that only searches for dash within a string to separate substrings
	re := regexp.MustCompile("[-]")
	// split all substrings based on regexp initialized above
	allElems := re.Split(input, -1)

	// regex blocking the numbers
	numRegex := regexp.MustCompile("[0-9]")

	// iterate over allElems and separate each element
	for _, element := range allElems {
		// check if element is a number
		isANumber := numRegex.MatchString(element)
		// perform further ops only if element is or does not have a number
		if !isANumber {
			// saves the element to a resultant string
			resultantString += " " + element
		}
	}

	// return the final string
	return resultantString
}

// StoryStats ->
//	Estimated Time to Complete : 20-30 minutes approx
//	Used time: 30-45 minutes including commenting
//	Difficulty: Easy
func StoryStats(input string) *StoryStatsPack {
	// shortestWord stores the shortest word
	var shortestWord string

	// longestWord stores the longest word
	var longestWord string

	// lists all words that have the average length
	var list = "" //nolint:gofumpt

	// avgLength stores the average length of word
	var avgLength = 0 //nolint:gofumpt

	// total length of words after adding up
	var totalWordsLength = 0 //nolint:gofumpt

	// counter to count total words in string
	var lenCounter = 0 //nolint:gofumpt
	var counter = 0
	// initialized regex that will be used to separate substrings
	re := regexp.MustCompile("[-]+")

	// regex blocking the numbers
	numRegex := regexp.MustCompile("[0-9]")

	// split all substrings based on regexp re that is declared above
	allElems := re.Split(input, -1)

	// iterate over allElems and separate each element
	for _, element := range allElems {
		// if element is a number, we will not execute the iteration further
		elementIsANum := numRegex.MatchString(element)
		// if it is not a number, then we will proceed
		if !elementIsANum {
			// if loop has just been initialized
			if counter == 0 {
				// save element to both shortestWord and longestWord variable
				shortestWord = element
				longestWord = element
			} else { // if loop has already been initialized
				// if length of current element is > length of the longest word
				// 		then element must be the longest word
				if len(element) > len(longestWord) {
					longestWord = element
				}
				// if length of current element is < length of the shortest word
				// 		then element must be the shortest word
				if len(element) < len(shortestWord) {
					shortestWord = element
				}
				// if multiple elements have same length and are largest, both must be the largest
				// if words are same, only one word will be counted
				if len(element) == len(longestWord) && element != longestWord {
					longestWord += " " + element
				}
				// if multiple elements have same length and are smallest, both must be the smallest
				// if words are same, only one word will be counted
				if len(element) == len(shortestWord) && element != shortestWord {
					shortestWord += " " + element
				}
			}
			// add up length of all words
			totalWordsLength += len(element)
			// increment the counter
			lenCounter++
			counter++
		}
	}
	// calculate the avg length of words
	avgLength = totalWordsLength / lenCounter
	// iterate over allElems and separate each element
	for _, element := range allElems {
		elementIsANum := numRegex.MatchString(element)
		if !elementIsANum {
			// if length of current element is +-1 ==  average length, then add the word to the list
			if len(element) == avgLength+1 || len(element) == avgLength-1 || len(element) == avgLength {
				list += " " + element
			}
		}
	}
	// packaging our story stats
	storyStatsPack := GetStoryStatsPack(avgLength, shortestWord, longestWord, list)

	// returns result
	return storyStatsPack
}

// StoryStatsPack packs the story in a single struct
type StoryStatsPack struct {
	AverageLength int    `json:"averageLength"`
	List          string `json:"list"`
	LongestWord   string `json:"longestWord"`
	ShortestWord  string `json:"shortestWord"`
}

// GetStoryStatsPack -> packaging our story stats into a struct
func GetStoryStatsPack(avgLen int, shortest, longest, list string) *StoryStatsPack {
	return &StoryStatsPack{
		AverageLength: avgLen,
		List:          list,
		LongestWord:   longest,
		ShortestWord:  shortest,
	}
}
