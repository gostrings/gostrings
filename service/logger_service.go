package service

import (
	"github.com/GoStrings/logger"
	"github.com/sirupsen/logrus"
)

type LoggerService interface {
	GetInstance() *logrus.Logger
}

type loggerService struct {
	logger *logrus.Logger
}

func NewLoggerService() LoggerService {
	logger.Init()
	return &loggerService{
		logger: logger.Instance(),
	}

}

func (log *loggerService) GetInstance() *logrus.Logger {
	return log.logger
}
