package service

import "github.com/GoStrings/models"

type StringsService interface {
	GetInputString(inputString models.InputRequest) (avg int64, wholeStory string, storyStats *models.StoryStatsPack, sr *models.StandardResponse)
}

type stringsService struct {
	config        GbeConfigService
	loggerService LoggerService
}

func NewStringsService(config GbeConfigService, loggerService LoggerService) StringsService {
	return &stringsService{
		config:        config,
		loggerService: loggerService,
	}
}

func (s stringsService) GetInputString(inputString models.InputRequest) (avg int64, wholeStory string, storyStats *models.StoryStatsPack, sr *models.StandardResponse) {
	// check if input is valid
	isValid := inputString.Validate()
	// if not valid, returns custom response with an error
	if !isValid {
		return 0, "", nil, models.NewStandardResponse(false, 400, "String is invalid", nil)
	}
	// Gets average number
	avg = models.AverageNumber(inputString.Input)

	// Gets whole Story
	wholeStory = models.WholeStory(inputString.Input)

	// Gets story stats
	storyStats = models.StoryStats(inputString.Input)

	return avg, wholeStory, storyStats, nil
}
