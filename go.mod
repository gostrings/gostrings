module github.com/GoStrings

go 1.17

require (
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/cors v1.2.0
	github.com/golang/mock v1.6.0
	github.com/rogierlommers/logrus-redis-hook v0.0.0-20210902125448-a9c74cf92631
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/garyburd/redigo v1.6.3 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
