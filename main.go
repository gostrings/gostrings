package main

import (
	"fmt"
	"github.com/GoStrings/logger"
	"github.com/GoStrings/rest"
	"github.com/GoStrings/service"
)

func main() {
	logger.Init()

	logger.Instance().Info("#==================================#")
	logger.Instance().Info("#===========Starting Server =======#")
	logger.Instance().Info("#==================================#")

	serviceContainer := service.NewServiceContainer()

	/*
	* Initiate Rest Server
	 */
	rest.StartServer(serviceContainer)

	fmt.Println("========== Rest Server Started ============")
	logger.Instance().Println("========== Server Stated ============")
	select {}

}
